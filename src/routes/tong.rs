use yew::prelude::*;
use crate::utils::spam_history::spam_history_with_tongicu;
use crate::utils::change_title;

/// Tong page
pub struct Tong;

impl Component for Tong {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        spam_history_with_tongicu();
        change_title("Tong");
        Tong {}
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="Tong mainTextDiv">
                {"Tong"}
            </div>
        }
    }
}
