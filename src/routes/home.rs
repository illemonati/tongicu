use yew::prelude::*;
use crate::utils::spam_history::spam_history_with_tongicu;

/// Home page
pub struct Home;

impl Component for Home {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        spam_history_with_tongicu();
        Home {}
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="home mainTextDiv">
                {"Tong See You"}
            </div>
        }
    }
}
