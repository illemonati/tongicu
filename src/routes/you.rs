use yew::prelude::*;
use crate::utils::spam_history::spam_history_with_tongicu;
use crate::utils::change_title;

/// You page
pub struct You;

impl Component for You {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        spam_history_with_tongicu();
        change_title("You");
        You {}
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="You mainTextDiv">
                {"You"}
            </div>
        }
    }
}
