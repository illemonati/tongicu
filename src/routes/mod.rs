use yew_router::prelude::*;
use yew_router::switch::Permissive;

pub mod home;
pub mod tong;
pub mod see;
pub mod home_no_spam;
pub mod you;

/// App routes
#[derive(Switch, Debug, Clone)]
pub enum AppRoute {
    #[to = "/page-not-found"]
    PageNotFound(Permissive<String>),
    #[to = "/homee"]
    HomeNoSpam,
    #[to = "/tong"]
    Tong,
    #[to = "/see"]
    See,
    #[to = "/you"]
    You,
    #[to = "/"]
    Home,
}
