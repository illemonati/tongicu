use yew::prelude::*;
use crate::utils::spam_history::spam_history_with_tongicu;

/// HomeNoSpam page
pub struct HomeNoSpam;

impl Component for HomeNoSpam {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        HomeNoSpam {}
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="HomeNoSpam mainTextDiv">
                {"Tong See You"}
            </div>
        }
    }
}
