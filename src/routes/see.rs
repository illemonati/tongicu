use yew::prelude::*;
use crate::utils::spam_history::spam_history_with_tongicu;
use crate::utils::change_title;

/// See page
pub struct See;

impl Component for See {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        spam_history_with_tongicu();
        change_title("See");
        See {}
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="See mainTextDiv">
                {"See"}
            </div>
        }
    }
}
