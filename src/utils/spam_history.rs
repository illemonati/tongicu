


pub(crate) fn spam_history_with_tongicu() {
    let window: web_sys::Window = web_sys::window().expect("Window No Existo");
    let history = window.history().expect("History No Existo");
    // let location = window.location();
    let js_tongcu = wasm_bindgen::JsValue::from("Tong See You");
    // location.set_href("/homee");
    // let og_href = location.href().expect("Href No Existo");
    let titles = ["Tong", "See", "You"];
    for i in 0..1000 {
        for j in 0..3 {
            let title = titles[j];
            history.push_state_with_url(&js_tongcu, title, Some(&format!("/{}", title)));
            let location = window.location();
            location.set_href(&format!("/{}", title));
        }
        history.go_with_delta(-i*3/2);
        history.forward();
        history.back();
        history.back();
        history.forward();
        window.open_with_url_and_target(&format!("/{}", titles[((i+1) % 3) as usize]), "_self");
        // history.go_with_delta(2);
    }

    history.push_state_with_url(&js_tongcu, "TSU", Some("/homee"));
    let len = history.length().unwrap();
    // history.go_with_delta(-((len-1) as i32));
    // let location = window.location();
    // location.set_href(&og_href);
    // let len = history.length().unwrap();
    // history.go_with_delta(len as i32);
}


