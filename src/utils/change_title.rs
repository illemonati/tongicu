
pub fn change_title(title: &str) {
    let window = web_sys::window().expect("window");
    let document = window.document().expect("document");
    document.set_title(title);
}


